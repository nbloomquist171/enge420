/*
 * Sofware program for the disc that moves the motors based on input
 * from the HC-12 module. The disc also send out accelerometer data.
 * Created by Noah Bloomquist.
*/


// Includes
#include <SoftwareSerial.h>
#include <Servo.h>
#include <BMI160Gen.h>
#include <Math.h>

// Objects
SoftwareSerial HC12(2, 3); // HC-12 TX Pin, HC-12 RX Pin
Servo myservo; // create servo object to control a servo

// Globals
int pos = 0;    // variable to store the servo position
char incomingByte;
String readBuffer = "";
int currentAngle = 0;
int lastAngle = 0;
int servo_start = 1;
const int select_pin = 10;
int gx, gy, gz;
double rpm;



// Setup for program
void setup()
{
  Serial.begin(9600);             // Serial port to computer
  HC12.begin(9600);               // Serial port to HC12
  myservo.attach(9); // attaches the servo on pin 9 to the servo object
  myservo.write(20);
  while (!Serial);
  BMI160.begin(BMI160GenClass::SPI_MODE, select_pin);
}

// Main Loop
void loop()
{
  get_joystick();
  set_aileron_angle();
  get_accelerometer();
  rotation_calculation();
  rpm_send();
  delay(60);
}

// Setting the aileron angle on the disc.
void set_aileron_angle()
{
  if(currentAngle==15)
  {
    myservo.write(90);
  }
  else if(currentAngle==19)
  {
    myservo.write(180);
  }
  else if(currentAngle==10)
  {
    myservo.write(0);
  }
}

// Gets the joystick reading from the HC-12 module
void get_joystick()
{
  readBuffer = "";
  boolean start = false;
  // Reads the incoming angle
  while (HC12.available())
  {
    incomingByte = HC12.read();          // Store each icoming byte from HC-12
    delay(5);
    // Reads the data between the start "s" and end marker "e"
    if (start == true) {
      if (incomingByte != 'e') {
        readBuffer += char(incomingByte);    // Add each byte to ReadBuffer string variable
      }
      else {
        start = false;
      }
    }
    // Checks whether the received message statrs with the start marker "s"
    else if ( incomingByte == 's') {
      start = true; // If true start reading the message
    }
  }
  // Converts the string into integer
  currentAngle = readBuffer.toInt();
}

// Get accelerometer data
void get_accelerometer()
{
  BMI160.readGyro(gx, gy, gz);
}

// RPM calculation
void rotation_calculation()
{
  rpm = sqrt(pow(gx, 2) + pow(gy, 2))/6;
}

// Send the RPMs to controller
void rpm_send()
{
  String angleString = String(int(rpm));
  //sends the angle value with start marker "s" and end marker "e"
  HC12.print("s" + angleString + "e");
}
