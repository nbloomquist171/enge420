/*
 * Sofware program for the disc golf controller that send the joystick data to the 
 * disc. Its also receives and displays the accelerometer data from the disc.
 * Created by Noah Bloomquist.
*/


// Includes
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SoftwareSerial.h>

// Defines
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define OLED_RESET     -1 // Reset pin # (or -1 if sharing Arduino reset pin)
#define NUMFLAKES     10 // Number of snowflakes in the animation example
#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16

// Objects
SoftwareSerial HC12(2, 3); // HC-12 TX Pin, HC-12 RX Pin
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Global Variables
char incomingByte;
String readBuffer = "";
const int dirPin = 4;
const int stepPin = 3;
const int button = 2;
int currentAngle = 0;
int lastAngle = 0;
int rotate = 0;
int VRx = A0;
int VRy = A1;
int SW = 5;
int xPosition = 0;
int yPosition = 0;
int SW_state = 0;
int mapX = 0;
int mapY = 0;
String a = "Setup";
int maxRpm = 0;
int rpm = 0;


// Setup
void setup() {
  Serial.begin(9600);
  
  // RF module Setup
  HC12.begin(9600);
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C))
  { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  // Display Setup
  display.display();
  delay(2000); // Pause for 2 seconds
  display.clearDisplay();
  display.setTextSize(1);             
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(32,32);
  display.print(a);
  display.display();

  // Set joystick pins as inputs
  pinMode(VRx, INPUT);
  pinMode(VRy, INPUT);
  pinMode(SW, INPUT); 
}

// Main Loop
void loop()
{
  rpm_receive();
  print_rpm();
  joystick_send();
  delay(40);
}

// Receive RPMs from the disc
void rpm_receive()
{
  readBuffer = "";
  boolean start = false;
  // Reads the incoming angle
  while (HC12.available())
  {
    incomingByte = HC12.read(); // Store each icoming byte from HC-12
    delay(5);
    // Reads the data between the start "s" and end marker "e"
    if (start == true) {
      if (incomingByte != 'e') {
        readBuffer += char(incomingByte); // Add each byte to ReadBuffer string variable
      }
      else {
        start = false;
      }
    }
    // Checks whether the received message statrs with the start marker "s"
    else if ( incomingByte == 's') {
      start = true; // If true start reading the message
    }
  }
  // Converts the string into integer
  rpm = readBuffer.toInt();
}

// Send the joystick data to the disc.
void joystick_send()
{
  xPosition = analogRead(VRx);
  mapX = map(xPosition, 0, 1023, 10, 20);
  String angleString = String(mapX);
  //sends the angle value with start marker "s" and end marker "e"
  HC12.print("s" + angleString + "e");
}

// Prints the RPMs on the OLED screen.
void print_rpm()
{
  // Clear the buffer
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(20,10);             // Start at top-left corner
  display.print(F("Dynamic Control")); 
  display.setCursor(46,22);
  display.print(F("Driver")); 
  display.setTextSize(2);             // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(25,40);             // Start at top-left corner
  display.print(F("RPM:"));
  display.setCursor(75,40); 
  display.print(rpm);
  display.display();
}

// Get maximum rpm.
void max_rpm()
{
  if (rpm > maxRpm)
  {
    maxRpm = rpm;
  }
}
